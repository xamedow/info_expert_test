var gulp         = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    sass         = require('gulp-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    spritesmith  = require('gulp.spritesmith'),
    imagemin     = require('gulp-imagemin'),
    svgmin       = require('gulp-svgmin'),
    plumber      = require('gulp-plumber'),
    concat       = require('gulp-concat'),
    pngquant     = require('imagemin-pngquant'),
    config       = {
        sprites   : {
            in : './img/sprites_in',
            out: '../img/sprites_out'
        },
        images_src: './img',
        scss_src  : './scss',
        css_dest  : './css'
    };


gulp.task('css', function () {
    return gulp.src(config.scss_src + '/init.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.css_dest));
});

gulp.task('svg', function () {
    gulp.src(config.images_src + '/svg/test.svg')
        .pipe(svgmin())
        .pipe(gulp.dest(config.images_src + '/svg'));
});

gulp.task('img_min', function () {
    gulp.src(config.images_src + '/*')
        .pipe(imagemin({
            progressive: true,
            use        : [pngquant()]
        }))
        .pipe(gulp.dest(config.images_src));
});

gulp.task('sprite', function () {
    // Generate our spritesheet
    var spriteData = gulp.src(config.sprites.in + '/**/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: config.sprites.out + '/sprite.png',
        cssName: '_sprite.scss',
        padding: 1
    }));

    // Pipe image stream through image optimizer and onto disk
    spriteData.img
        .pipe(imagemin())
        .pipe(gulp.dest(config.sprites.out));

    // Pipe CSS stream through CSS optimizer and onto disk
    spriteData.css
        .pipe(gulp.dest(config.scss_src + '/services/'));
});

gulp.task('watch', function () {
    gulp.watch(config.sprites.in + '/**/*', ['sprite']);
    gulp.watch(config.scss_src + '/**/*.scss', ['css']);
});

gulp.task('default', ['css']);