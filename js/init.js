(function () {
    'use strict';
    var dropdownTrigger = $('.dropdown-button'),
        dropdown = $('.dropdown'),
        toggleClass;
// Fancy box
    $('.fancybox').fancybox();

// Dropdown menu
    if (dropdown && dropdownTrigger) {
        dropdownTrigger.click(function (e) {
            e.preventDefault();
            dropdownTrigger.toggleClass('active');
            dropdown.slideToggle();
        });
    }
} ());